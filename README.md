# LLM-Powered Chat Assistant with Retrieval Augmented Generation (RAG)

Developed an LLM-powered conversational assistant which loads and index data from Wikipedia, enabling it to deliver factual responses based on retrieved Wikipedia pages.

## Overview
Integrated Anthropic's Claude 3 models with RAG pipeline, Llamaindex for context building, and Chainlit for the conversation AI user interface  to create a conversational assistant capable of providing informed and contextually relevant responses. By ingesting data from Wikipedia's knowledge base, the assistant becomes skilled at answering queries with accuracy and depth.

Data loaded and indexed can be gotten from our data source with a change in a few lines of code. eg. A use can would be for a private knowledge base and domain. 

## Demo
<img src="demo.gif" width="1400" height="850" />

## Key Components
- **LLM Library**: Implemented Anthropic's Claude 3 models o fuel our conversational assistant language processing capabilities.

- **Llamaindex**: For context building by applying Claude 3 on top of indexed data. Utilize Hugging Face embedding models within Llamaindex to represent documents with numerical representations, enabling semantic search for relevant information.

- **Retrieval Augmented Generation (RAG)**: Improve the accuracy of  the outputs of the LLM by incorporating factual information from the Wikipedia knowledge base. This involves indexing Wikipedia data to efficiently retrieve relevant context for user queries.

- ReAct Prompt Framework: conversational agent operates within the ReAct prompt framework, enabling a systematic approach to answering questions. This framework guides the agent through sequential steps, facilitating optimal responses to user inquiries.


## Technology Used
<p align="left"><a href="https://www.python.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a><a href="https://www.anthropic.com/claude" target="_blank" rel="noreferrer"> <img src="https://www.anthropic.com/_next/image?url=https%3A%2F%2Fcdn.sanity.io%2Fimages%2F4zrzovbb%2Fwebsite%2F1c42a8de70b220fc1737f6e95b3c0373637228db-1319x1512.gif&w=3840&q=75" alt="clause" width="50" height="50"/> <a href="https://docs.chainlit.io/get-started/overview" target="_blank" rel="noreferrer"> <img src="https://www.educative.io/api/project_tag/image/4847372350914560" alt="chainlit" width="40" height="40"/>
<a href="https://docs.llamaindex.ai/" target="_blank" rel="noreferrer"> <img src="https://www.educative.io/api/project_tag/image/5439421664067584" alt="llama-index" width="60" height="60"/> 
<a href="https://docs.pydantic.dev/latest/" target="_blank" rel="noreferrer"> <img src="https://avatars.githubusercontent.com/u/110818415?s=200&v=4" alt="pydantic" width="40" height="40"/>  
<a href="https://huggingface.co/" target="_blank" rel="noreferrer"> <img src="https://static-00.iconduck.com/assets.00/hugging-face-emoji-1014x1024-f8v9nfxv.png" alt="huggingface" width="40" height="40"/> 
</p>



## Setup
1. Clone the Repository: `git clone https://gitlab.com/ml-projects4363519/llm-conversational-agent-with-rag.git`

2. Install Dependencies: `pip install --upgrade pip; pip install -r requirements.txt`

3. Explore Codebase and start application on localhost: `chainlit run chat_app.py`

